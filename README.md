A responsive email template that uses the 'fluid hybrid' approach without any media queries and 100% compatible across all devices and email clients.

## Running for development

For development, I suggest using some sort of server such as [http-server](https://github.com/http-party/http-server) within the root of this directory

## Buiding for production

If your email sending platform doesn't take care of inlining for you then you’ll need to do it manually. First, delete the link tag `<link rel="stylesheet" type="text/css" href="styles.css" />` in the `<head>` of your document and replace it with `<style type="text/css">. Copy the contents of style.css and paste it underneath, then close the style tag with </style>`. Finally, copy and paste the entire file into the box at [inliner.cm](https://www.campaignmonitor.com/resources/tools/css-inliner/)  and wait for the results. Once processed, copy the contents out of the box and you’re ready to go!
